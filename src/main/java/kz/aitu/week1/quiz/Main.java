package kz.aitu.week1.quiz;

import kz.aitu.week1.Practice1;
import kz.aitu.week1.Practice2;
import kz.aitu.week1.Practice3;


public class Main {

    public static void main(String[] args) {
        Practice1 practice1 = new Practice1();
        Practice2 practice2 = new Practice2();
        Practice3 practice3 = new Practice3();
        Labirint Labirint = new Labirint();
        Labirint.run();
    }
}

package kz.aitu.Midterm;

public class Node {
      int key;
    String value;
    Node left;
    Node right;

    public Node(int key, String value){
        this.key=key;
    this.value=value;
    this.left=null;
    this.right=null;

    }

    public Node getLeft() {
        return left;
    }
    public Node getRight(){
        return right;
    }
    public void setLeft(Node left){
        this.left= left;
    }
    public void setRight(Node right){
        this.right= right;
    }


    public int getKey(){
        return key;
    }

    public String getValue(){
        return value;
    }

    public void setKey(int key){
        this.key= key;
    }

    public void setValue(String value){
        this.value= value;
    }

}

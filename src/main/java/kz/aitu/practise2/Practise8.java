package kz.aitu.practise2;
import java.util.Scanner;
public class Practise8 {

    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        int n=scanner.nextInt ();

        String a[][] = new String[n+1][n+1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextLine();
            }
        }

        boolean flag = false;
        //flag=findPath(n, m, 0, 0, flag, a );
        print(n,a  ,flag);
    }

    public boolean findPath(int n, int m, int i,int j, boolean flag , String a[][]) {
        if (a[i][j] == "#") return false;

        if (i == n-1 && j == m-1) {
            a[i][j] = "A";

            return   true;
        }


        if  (i + 1 != n)
            flag = flag || findPath(n, m, i + 1, j, flag, a);
        return findPath2(n, m, i , j, flag, a);
    }

    public boolean findPath2(int n, int m, int i,int j, boolean flag , String a[][]) {
        if (i != 0) flag = flag || findPath(n, m, i - 1, j+1, flag, a);
        return findPath3(n, m, i , j, flag, a);}

    public boolean findPath3(int n, int m, int i,int j, boolean flag , String a[][]) {
        if (j + 1 != m) flag = flag || findPath(n, m, i, j + 1, flag, a);
        return findPath4(n, m, i , j, flag, a);}

    public boolean findPath4(int n, int m, int i,int j, boolean flag , String a[][]) {
        if (j != 0) flag = flag || findPath(n, m, i+1, j - 1, flag, a);

        return flag;

    }


    public void print(int n,String a[][],boolean flag) {
        for (int i = 0; i < n+1; i++) {
            for (int j = 0; j < n+1; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        if(flag){ System.out.println("YES");} else {System.out.println("NO");}
    }

    public void run() {
        inputData();
        }
    }


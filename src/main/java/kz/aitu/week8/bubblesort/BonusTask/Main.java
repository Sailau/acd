package kz.aitu.week8.bubblesort.BonusTask;

import static kz.aitu.week8.bubblesort.BonusTask.BFS.BFS;

public class Main {

    //+200points for ADS
    public static void main(String[] args) {
        checkTask();
    }


    public static void checkTask() {
        BonusPoints bonusPoints = new BonusPoints();

        bonusPoints.addRoot("Student1");
        bonusPoints.addChild("Student2", "Student1");
        bonusPoints.addChild("Student3", "Student2");
        bonusPoints.addChild("Student4", "Student3");
        bonusPoints.addChild("Student5", "Student3");
        bonusPoints.addChild("Student6", "Student5");
        bonusPoints.addChild("Student7", "Student5");
        bonusPoints.addChild("Student11", "Student2");

        bonusPoints.addRoot("Student10");
        bonusPoints.addChild("Student20", "Student10");
        bonusPoints.addChild("Student31", "Student10");

        bonusPoints.addChild("Student80", "Student10");//если больше трех не примет

        bonusPoints.addChild("Student30", "Student20");


        bonusPoints.addRoot("Student15");
        bonusPoints.addChild("Student2", "Student15");//если студент уже сещуствует игнорирует

        bonusPoints.addChild("Student25", "Student15");
        bonusPoints.addChild("Student35", "Student15");


        bonusPoints.addChild("Student45", "Student15");//если больше трех не примет

        bonusPoints.addChild("Student35", "Student25");

        bonusPoints.addRoot("Student1");//если рут существует не добавит пропустит

        bonusPoints.addChild("7", "Student1");

        bonusPoints.addRoot("Student10");//если рут существует не добавит пропустит

        bonusPoints.addChild("Student35", "Student80");// если добавляет к несуществующему студенту игнорирует


        BonusPoints BFS = new BonusPoints();
        BFS("Student 20");// Ну это не бонус, а новая тема

        System.out.println(bonusPoints.getPoints("Student1"));
        System.out.println(bonusPoints.getPoints("Student20"));
        System.out.println(bonusPoints.getPoints("Student15"));
        System.out.println(bonusPoints.getPoints("Student3"));
        System.out.println(bonusPoints.getPoints("Student5"));

        System.out.println(bonusPoints.getPoints("Student80"));// если не существует то покажет 0

        System.out.println(bonusPoints.getPoints("Student25"));
        System.out.println(bonusPoints.getPoints("Student10"));
    }

}

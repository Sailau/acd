package kz.aitu.week8.bubblesort.BonusTask;


public class NodeQ {
    private Integer key;
    private String value;
    private NodeQ left;
    private NodeQ right;

    NodeQ next;
    String data;

    public NodeQ(String data){
        this.data = data;
    }

    public String data() {
        return data;
    }


    public void setData(String data) {
        this.data = data;
    }

    public NodeQ next() {
        return next;
    }

    public void setNext(  NodeQ next) {
        this.next = next;
    }

    public String toString(){
        return this.data;
    }



    public NodeQ(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public NodeQ getLeft() {
        return left;
    }

    public void setLeft(NodeQ left) {
        this.left = left;
    }

    public NodeQ getRight() {
        return right;
    }

    public void setRight(NodeQ right) {
        this.right = right;
    }
}

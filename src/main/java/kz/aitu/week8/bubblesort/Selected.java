package kz.aitu.week8.bubblesort;

import java.util.Scanner;

public class Selected {
    public static void main(String[] args) {
        int arr[]={1,5,4,9,95,1,35,4,4,40};
        Scanner scan = new Scanner(System.in);
        range( arr);
    }

    public static void range(int arr[]) {

        {
            int n = arr.length;

            for (int i = 0; i < n - 1; i++) {
                int max_ind = i;
                for (int j = i + 1; j < n; j++)
                    if (arr[j] > arr[max_ind])
                        max_ind = j;

                int temp = arr[max_ind];
                arr[max_ind] = arr[i];
                arr[i] = temp;
            }
        }
    }

}

package kz.aitu.week8.bubblesort;

public class quick {
    public static void main(String[] args) {
        int x[]={38,3,1,95,2,15};
        sort( x);
        for(int i=0; i<x.length; i++){
            System.out.println(x[i]);
        }
    }

    public static void sort(int[] x) {
        quicksort(x,0,x.length-1);
    }

    public static void quicksort(int[] x,int start, int end ) {
if(start<end){
    int pi=partition(x,start,end);
    quicksort(x, start,pi-1 ); // sort left half
    quicksort(x, pi+1 , end ); // sort right half
}

    }

private static int partition( int[] x, int start, int end){
int pivot=x[end];
int k=start-1;

for(int i=start; i<end; i++) {
    if (x[i] < pivot) {   k++;
        int temp = x[k];
        x[k] = x[i];
        x[i] = temp;

    }
}
        int temp = x[k+1];
        x[k+1] =x[end];
        x[end] = temp;

return k+1;
}

}

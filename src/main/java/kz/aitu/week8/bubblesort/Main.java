package kz.aitu.week8.bubblesort;
import java.util.Random;

public class Main {

        public static void main(String[] args) {
            int[] arr = new int[20];
            inputData(arr);


            select selection = new select();
            insertion insertion = new insertion();
            merge merge = new merge();
            quick quick = new quick();

            System.out.println("MY DATA");
            printAll(arr);

            System.out.println("SELECTION SORTED===========================");
            selection.sort(arr);
            printAll(arr);

            System.out.println("INSERTION SORTED===========================");
            int[] arr2 = new int[20];
            inputData(arr2);
            insertion.sort(arr2);
            printAll(arr2);

            System.out.println("MERGE SORTED===========================");
            int[] arr3 = new int[20];
            inputData(arr3);
            merge.sort(arr3);
            printAll(arr3);

            System.out.println("QUICK SORTED");

            int[] arr4 = new int[20];
            inputData(arr4);
            quick.sort(arr4);
            printAll(arr4);
        }

        private static void printAll(int[] array) {
            System.out.print("==> ");
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i]);
            }
            System.out.println();
        }


        private static void inputData(int[] array) {
            Random r = new Random();
            for (int i = 0; i < array.length; i++) {
                array[i] =  r.nextInt(10);
            }
        }
    }



package kz.aitu.week9.BFSandDFS;

public class Stack {
    private StackNode top;
    private int size = 0;

    public void setTop(StackNode top) {
        this.top = top;
    }

    public StackNode getTop() {
        return top;
    }

    public void Push(int value){
        StackNode new_node = new StackNode(value);
        if (top == null) {
            top = new_node;
            size++;
        }
        else {
            new_node.setNextTop(top);
            top = new_node;
            size++;
        }
    }


    public int Pop(){
        StackNode c = top;
        top = top.getNextTop();
        size--;
        return c.getValue();
    }

    public boolean empty(){
        if (top == null) return true;
        else return false;
    }
    public int size(){
        return size;
    }
}

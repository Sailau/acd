package kz.aitu.week9.BFSandDFS;

public class Node {
    private int key;
    private String value;
    private Node left;
    private Node right;

    public Node(int key, String value) {
        this.key = key;
        this.value = value;
        this.right = null;
        this.left = null;
    }

    public String getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}

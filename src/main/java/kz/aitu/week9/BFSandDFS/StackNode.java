package kz.aitu.week9.BFSandDFS;

public class StackNode {
    private int value;
    private StackNode nextTop;

    public StackNode(int value) {
        this.value = value;
    }

    public void setNextTop(StackNode nextTop) {

        this.nextTop = nextTop;
    }

    public void setValue(int value) {

        this.value = value;
    }

    public int getValue() {

        return value;
    }

    public StackNode getNextTop() {

        return nextTop;
    }
}

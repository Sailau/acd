package kz.aitu.week9.BFSandDFS;

public class Main {
    public static void main(String[] args) {
        tree bsTree = new tree();

        bsTree.put(1000, "A");
        bsTree.put(2000, "B");
        bsTree.put(500, "C");
        bsTree.put(1500, "D");
        bsTree.put(750, "E");
        bsTree.put(250, "F");
        bsTree.put(625, "G");
        bsTree.put(1250, "H");
        bsTree.put(875, "I");
        bsTree.put(810, "Z");

        System.out.println("===Print ALL(A C B F E D G I H Z)");
        bsTree.BFS();

        System.out.println();

        System.out.println("===Print ALL(A C F E G I Z B D H)");
        bsTree.DFS();

    }

}

package kz.aitu.week5.Midterm.Stack;

public class Node {
    Node next;
    String data;

    public Node(String data){
        this.data = data;
    }

    public String data() {
        return data;
    }


    public void setData(String data) {
        this.data = data;
    }

    Node next() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public String toString(){
        return this.data;
    }


    public static void  push_back( String n, Node temp) {

        Node a = new  Node(n);
        if (temp.next != null) {
            temp = temp.next;
            push_back(n, temp);

        }if(temp.next==null){temp.setNext(a); }


        return;
        //O(n)
    }
}

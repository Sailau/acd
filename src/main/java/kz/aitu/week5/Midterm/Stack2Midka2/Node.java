package kz.aitu.week5.Midterm.Stack2Midka2;

public class Node {
    Node next;
    int data;

    public Node(int data){
        this.data = data;
    }

    public int data() {
        return data;
    }


    public void setData(int data) {
        this.data = data;
    }

    Node next() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }



    public static void  push_back( int n, Node temp) {

        Node a = new Node(n);
        if (temp.next != null) {
            temp = temp.next;
            push_back(n, temp);

        }if(temp.next==null){temp.setNext(a); }


        return;
        //O(n)
    }

}

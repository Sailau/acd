package kz.aitu.week5.Midterm;

public class Node {
    public Node next;

     String data;

    public Node(String data){
        this.data = data;
    }

    public String data() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Node next() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

 }

package kz.aitu.week7.additional.bst;


public class Node {
    private Integer key;
    private String value;
    private Node left;
    private Node right;

    Node next;
    String data;

    public Node(String data){
        this.data = data;
    }

    public String data() {
        return data;
    }


    public void setData(String data) {
        this.data = data;
    }

    public  Node next() {
        return next;
    }

    public void setNext(  Node next) {
        this.next = next;
    }

    public String toString(){
        return this.data;
    }



    public Node(Integer key, String value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}

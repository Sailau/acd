package kz.aitu.week7.additional.bst;

public class BSTree {
    private Node root;
    int size;
    Queue queue = new Queue();

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node a1 = new Node(key, value);

        if(root == null) root=a1;
        queue.add(a1);
        Node b = root;
size++;
        while(b!=null){
            if(b==a1) break;

          if(b.getKey()>a1.getKey()) {
             if(b.getLeft()==null){
                 b.setLeft(a1);
                 queue.add(a1);
                 break;
             }else{
    b = b.getLeft();}
}

      if(b.getKey()<a1.getKey()) {
          if(b.getRight()==null){b.setRight(a1);
              queue.add(a1);
              break;
          }else{
              b = b.getRight();}
      }

}
     }

    public boolean delete(Integer key) {
Node b= root;
Boolean t =false;
Node d= findNode(root,key);

        if(find(key) == null)
        {  return false;}
        else { if(d.getKey()==root.getKey()){
            if(root.getLeft()!=null) {
                Node temp=d;
                d = d.getLeft();
                while(d.getRight()!=null){
                    d = d.getRight();
                     temp=d;
                        }
                {if (null != b.getLeft() && d.getKey() < root.getKey()) {
                    while (d.getValue() != b.getLeft().getValue()) {
                        if (b.getKey() > d.getKey()) {
                            b = b.getLeft();
                            if(d.getKey()>b.getKey()){
                                while ( d.getValue() != b.getRight().getValue()) {
                                    b = b.getRight();
                                    break;
                                }t= true; break;}
                        } } }

                    if (null != b.getRight() && d.getKey() > b.getKey() && t==false) {
                        while (  d.getValue() != b.getLeft().getValue()) {
                            if (b.getKey() < d.getKey()) {
                                b = b.getRight();
                            } }
                    }
                if (d.getLeft() != null) {
                    d = d.getLeft();
                    b.setRight(d);
                            }
                temp.setRight(root.getRight());
                temp.setLeft(root.getLeft());
                root=temp;
                b=root;
            }}else{


                Node temp=d;
                d = d.getRight();
                while(d.getLeft()!=null){
                    d = d.getLeft();
                    temp=d;
                }

                {if (null != b.getLeft() && d.getKey() < root.getKey()) {
                    while (d.getValue() != b.getLeft().getValue()) {
                        if (b.getKey() > d.getKey()) {
                            b = b.getLeft();
                            if(d.getKey()>b.getKey()){
                                while ( d.getValue() != b.getRight().getValue()) {
                                    b = b.getRight();
                                    break;
                                }t= true; break;}

                        } } }

                    if (null != b.getRight() && d.getKey() > b.getKey() && t==false) {
                        while (  d.getValue() != b.getLeft().getValue()) {
                            if (b.getKey() < d.getKey()) {
                                b = b.getRight();
                            } }
                    }}

                    if (d.getLeft() != null) {
                        d = d.getLeft();
                        b.setLeft(d);
                    }
                    temp.setRight(root.getRight());
                temp.setLeft(root.getLeft());
                root=temp;
                b=root;
            }
        }else{
        {if (null != b.getLeft() && d.getKey() < root.getKey()) {
                    while (d.getValue() != b.getLeft().getValue()) {
                        if (b.getKey() > d.getKey()) {
                            b = b.getLeft();
                           if(d.getKey()>b.getKey()){
                            while ( d.getValue() != b.getRight().getValue()) {
                                b = b.getRight();
                                break;
                            }t= true; break;}

                            } } }

                if (null != b.getRight() && d.getKey() > b.getKey() && t==false) {
                    while (  d.getValue() != b.getLeft().getValue()) {
                        if (b.getKey() < d.getKey()) {
                            b = b.getRight();
                        } }
                }

            if (d.getLeft() == null && d.getRight() == null) {
                if (d.getKey() < root.getKey()) {
                    d = null;
                    b.setLeft(d);}

                    if ( d!=null && d.getKey() > root.getKey() ) {
                        d = null;
                        b.setRight(d); }
            } else
                if(d.getLeft() != null && d.getRight() != null) {

  while(d.getLeft().getLeft()!=null){
     d=d.getLeft();
     d=d.getRight();

}

    if (d.getLeft() != null) {
        Node right = d.getRight();

        d=d.getLeft();
        d.setRight(right);

        b.setLeft(d);
    } } else {

                if (d.getKey() > root.getKey()) {
                    if (d.getLeft() != null) {
                        d = d.getLeft();
                        b.setLeft(d);
                    } else if (d.getRight() != null) {

                        d = d.getRight();
                        b.setRight(d);
                    }
                }

                if (d.getKey() < root.getKey()) {
                    if (d.getLeft() != null) {
                        d = d.getLeft();
                        b.setRight(d);
                    }
                    if (d.getRight() != null) {
                        d = d.getRight();
                        if (d.getKey() > b.getKey()) {
                            b.setRight(d);
                            return true;
                        }
                        else{
                        b.setLeft(d); }}
                } }}}
        }return true;

    }



    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending( ) {

         Node b=root;

        while(root.getLeft()!=null ){
            if(  root.getKey()>b.getKey() && b.getRight().getKey()>root.getKey() && root.getRight()==null) {
                root=b.getRight();
                System.out.print(root.getValue());
                return;}
            if(root.getRight()==null && root.getKey()<b.getKey()){
                root=b.getRight();
                System.out.print(b.getValue());
            }
     root=root.getLeft();
    printAllAscending( );


}
        root=b;
 System.out.print(root.getValue());

        if(root.getRight()!=null){
            root=root.getRight();
            printAllAscending();

 } return; }

    public void printAll() {

        if (root == null) { return;}
        Node x = queue.head;
        Node y=queue.head;
        y=y.getRight();

        System.out.print(x.getValue() + " ");
        Node c=null;

        while (x!=null) {
              for (int i = 0; i < size; i++) {

                  if(x!=null) {
                      if (x.getLeft() != null) {
                          System.out.print(x.getLeft().getValue() + " ");
                      }
                      if (x.getRight() != null) {
                          System.out.print(x.getRight().getValue() + " ");
                          c = x.getRight();
                      }
                  }
if( x==null ||(c!=null && x.getRight()==null) ){
if(x!=null){x=c.getLeft();}
    if (c.getLeft()!= null) {
        System.out.print(c.getLeft().getValue() + " ");
    }
    if (c.getRight()!= null) {
        System.out.print(c.getRight().getValue() + " ");
        c=c.getRight();
    }
}
                  if(x==queue.head){ x=x.getLeft(); break;}
                  if(x!=null){ x=x.getLeft();}else{break;}


                 if (y.getLeft() != null) {
                     System.out.print(y.getLeft().getValue() + " ");
                     Node b=y.getLeft();
                 }
                 if (y.getRight() != null) {
                     System.out.print(y.getRight().getValue() + " ");
                     Node m=y.getRight();
                 }
                 y=y.getLeft();


             }

            // new level
          }



}}


package kz.aitu.week6.hash.heap;

import java.util.NoSuchElementException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Heap heap = new Heap(12);

        heap.insert(4);
        heap.insert(1);
        heap.insert(5);
        heap.insert(2);
        heap.insert(6);
        heap.insert(3);
        heap.insert(7);
        heap.insert(12);
        heap.insert(8);
        heap.insert(11);
        heap.insert(9);
        heap.insert(10);

        System.out.print("Heap is: ");
        heap.printHeap();
        heap.delete(8);
        System.out.print("New heap is: ");
        heap.printHeap();
    }

    public static class Heap{
        private static final int a = 2;
        public int[] heap;
        public int size;

        public Heap(int a){
            size = 0;
            heap = new int[a + 1];
            Arrays.fill(heap, -1);
        }

        public boolean isFull(){
            return size == heap.length;
        }

        private void heapifyUp(int i) {
            int temp = heap[i];
            while(i > 0 && temp > heap[parent(i)]){
                heap[i] = heap[parent(i)];
                i = parent(i);
            }
            heap[i] = temp;
        }

        public void insert(int d){
            if(isFull()) throw new NoSuchElementException("Heap is full, you can't add the element !");
            heap[size++] = d;
            heapifyUp(size-1);
        }

        private int parent(int i){
            return (i-1)/a;
        }

        private int cChild(int i, int c){
            return a * i + c;
        }

        public boolean isEmpty(){
            return size == 0;
        }

        private void ifHeapDown(int i){
            int childElement;
            int temp = heap[i];
            while(cChild(i, 1) < size){
                childElement = maxChild(i);
                if(temp < heap[childElement]){
                    heap[i] = heap[childElement];
                }else break;
                i = childElement;
            }
            heap[i] = temp;
        }

        private int maxChild(int i){
            int leftChild = cChild(i, 1);
            int rightChild = cChild(i, 2);
            return heap[leftChild] > heap[rightChild]
                    ?leftChild:rightChild;
        }

        public int delete(int d){
            if(isEmpty()) throw new NoSuchElementException("Heap is empty, there is no elements to delete !");
            int key = heap[d];
            heap[d] = heap[size - 1];
            size--;
            ifHeapDown(d);
            return key;
        }

        public void printHeap(){
            for (int i = 0; i < size; i++){
                System.out.print(heap[i] + " ");
            }
            System.out.println();
        }
    }
}
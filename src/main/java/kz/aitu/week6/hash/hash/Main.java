package kz.aitu.week6.hash.hash;

public class Main {
    public static void main(String[] args) {
        String[] users = {"User1", "User2", "User3", "User4", "User5", "User6", "User7", "User8", "User9", "User10" };
        String[] usersNew = new String[20];

        for (int i = 0; i < usersNew.length; i++){
            usersNew[i] = " ";
        }

        for (int i = 0; i < users.length; i++){
            System.out.println(users[i] + ": " + hashFunction(users[i], usersNew));
        }

        for (int i = 0; i < usersNew.length; i++){
            if (usersNew[i] != ""){
                System.out.print(usersNew[i] + " [" + i + "]" + ", ");
            }
        }
        System.out.println("");

        for (int i = 0; i < users.length; i++){
            System.out.println(users[i] + ": " + getIndex(users[i], usersNew)); }
    }

    public static int hashFunction(String userName, String[] usersNew){
        int index = 0;
        int arraySize = usersNew.length -5;

        for (int i = 0; i < userName.length(); i++){
            char c = userName.charAt(i);
            index += (int)c;
        }

        index = index % arraySize;

        if (usersNew[index] == " ") usersNew[index] = userName;
        else if (usersNew[index] != " "){
            for (int j = 1; j < (usersNew.length - index); j++){
                if (usersNew[index+j] == ""){
                    index = index + j;
                    usersNew[index] = userName;
                    break; } } }
        return index;
    }

    public static int getIndex(String userName, String[] usersNew){
        int index = 0;
        int arraySize = usersNew.length -5;

        for (int i = 0; i < userName.length(); i++){
            char c = userName.charAt(i);
            index += (int)c;
        }

        index = index % arraySize;
        if (usersNew[index] == userName){
            return index;
        }else{
            for (int j = 1; j < (usersNew.length - index); j++){
                if (usersNew[index+j] == userName){
                    index = index + j;
                    return index; } } }

        return -1;
    }
}

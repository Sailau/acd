package kz.aitu.week6.hash.tree;

public class Node {
    int value;
    Node left, right;

    Node(int value){
        this.value = value;
        left = null;
        right = null;
    }
}

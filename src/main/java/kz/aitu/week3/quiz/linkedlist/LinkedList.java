package kz.aitu.week3.quiz.linkedlist;

public class LinkedList {
    private static Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }
    public static void printAllNodes(Node head){
        Node current = head;
        while(current != null) {
            System.out.println(current.getData());
            current = current.getNext();
        }
    }
    public static void size(Node head){
        int size=0;
        Node current1 = head;
        while(current1!=null){
            size++;
            current1=current1.getNext();
        }
        System.out.println("Size="+ size);
    }


    public static void insertIntoIndex(Node head, Node node7, int index){
        Node a = head;

        for(int i =0;(index-1)!=i;i++)   {
            a=a.getNext() ;
        }
        node7.setNext(a.getNext()); a.setNext(node7);
    }

    public static void addAtFront(Node head, Node n){
        n.setNext(head);
    }

    public static void addAtEnd(Node node8, Node node9) {


        Node a = node8;
        while (a.getNext() != null) {
            a = a.getNext();
        }

        a.setNext(node9);
        node9.setNext(null);
    }

    public  static void deleteHead( ){
           head=head.getNext();

    }

    public  static void deleteEnd(Node head){

        while(head.getNext().getNext()!=null) {
            head = head.getNext();
        }
        head. setNext(null) ;
    }

}

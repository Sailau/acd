package kz.aitu.practise;
import java.util.Scanner;

public class PractiseNine2 {
    public static int  binomialCoeff(int n, int k)
    {
        if (k == 0 || k == n)
            return 1;
        return binomialCoeff(n - 1, k - 1) +
                binomialCoeff(n - 1, k);
    }


    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt(), k = s.nextInt();
        System.out.println(binomialCoeff(n,k));
    }
}

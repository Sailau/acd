package kz.aitu.practise;
import java.util.Scanner;

public class PractiseSeven {

    public void counter(int c){
        if(c==0) return ;

        Scanner s = new Scanner(System.in);
        int b=s.nextInt();

        c=c-1;
        counter(c);
        System.out.println(b);

    }
    public  void run() {
        Scanner s = new Scanner(System.in);
        int c = s.nextInt();
        counter(c);
    }
}

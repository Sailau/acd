package kz.aitu.practise;
import java.util.Scanner;


public class PractiseTen {

    public static int  Eucli(int x, int y)
    {
        int temp = y;
        y=x%y;
        x=temp;
        if (y==0)
            return x;
        else{
            return Eucli(x,y);
        }

    }


    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt(), y = s.nextInt();
        System.out.println(Eucli(x,y));
    }

}





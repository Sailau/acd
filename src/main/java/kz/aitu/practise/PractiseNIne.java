package kz.aitu.practise;

import java.util.Scanner;
public class PractiseNIne {

    public void run() {
        int n, k;
        Scanner scan = new Scanner(System.in);
        n = scan.nextInt();
        k = scan.nextInt();

        int b = fact(n);
        int e = fact(k);
        int f = fact(n - k);

        System.out.println(bin(b, e, f));
    }

    int fact(int n) {
        if (n == 1 || n == 0)
            return 1;
        else {
            n = fact(n - 1) * n;
            return n;
        }
    }

    int bin(int b, int e, int f) {
        int result = b / (e * f);
        return result;
    }
}

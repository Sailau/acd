package kz.aitu.practise;
import java.util.Scanner;

public class PractiseSix {

    public void run() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int a = scanner.nextInt();
        int c = 1;
        long count = 0;
        power(n, a, c, count);
    }

    void power(int n, int a, int c, long count) {
        int b;
        if (count < n) {
            c *= a;
            if (count == n - 1) {
                System.out.println(c);
            }

            power(n, a, c, count + 1);

        }
    }
}

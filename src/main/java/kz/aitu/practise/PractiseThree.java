package kz.aitu.practise;

import java.util.Scanner;

public class PractiseThree{

    public boolean isPrime(int n, int k) {
        if (n==k) return true;
        else if (n % k == 0) return false;

        return isPrime(n, k + 1);


    }


    public void run() {
        int n;

        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();

        if (isPrime(n, 2)) System.out.println("Prime");
        else System.out.println("Composite");
    }
    }


